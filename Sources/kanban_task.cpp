#include <QDebug>

#include "kanban_store.h"
#include "kanban_task.h"
#include "ui_kanban_task.h"

/**
 * Constructor
 */
KanbanTask::KanbanTask(const qlonglong &id, const qlonglong &rowId, const QString &title, QWidget *parent) :
	QWidget(parent),
	ui(new Ui::KanbanTask)
{
	ui->setupUi(this);
    ui->name->setText(title);

    this->id = id;
    this->rowId = rowId;
    this->name = title;

	connect(ui->doneButton, &QPushButton::clicked, this, &KanbanTask::done);
    connect(ui->undoneButton, &QPushButton::clicked, this, &KanbanTask::undone);
    connect(ui->deleteButton, &QPushButton::clicked, this, &KanbanTask::remove);
}

/**
 * Destructor
 */
KanbanTask::~KanbanTask()
{
	delete ui;
}

/**
 * Sets the stroe
 * @param store store
 */
void KanbanTask::setStore(KanbanStore *store)
{
	this->store = store;
}

/**
 * Returns the store
 * @return Store
 */
KanbanStore* KanbanTask::getStore()
{
	return store;
}

/**
 * Returns the ID of the task
 * @return ID of the task
 */
qlonglong KanbanTask::getId() const
{
	return id;
}

/**
 * Sets the ID of the row in which the task is.
 * @param id ID of the parent row.
 */
void KanbanTask::setRowId(const qlonglong &id)
{
	this->rowId = id;
}

/**
 * Returns the parent row id.
 * @return ID of the parent row.
 */
qlonglong KanbanTask::getRowId() const
{
	return rowId;
}

/**
 * Set the position of the parent row.
 * @param position position of the parent row.
 */
void KanbanTask::setPosition(const qlonglong &position)
{
    this->position = position;
}

/**
 * Get the position of the parent row.
 * @return Position of the parent row.
 */
qlonglong KanbanTask::getPosition() const
{
    return position;
}

/**
 * Sets the name of a task
 * @param name Name of the task
 */
void KanbanTask::setName(const QString &name)
{
	this->name = name;
}

/**
 * Returns the name of the task
 * @return Name of the task
 */
QString KanbanTask::getName() const
{
	return name;
}

/**
 * Set the QVBox of the parent row in order to move task.
 * @param list QVBox
 */
void KanbanTask::setLayout(QBoxLayout *list)
{
	this->list = list;
}

/**
 * Hides or Shows the done button
 * @param visible True show button | False hide button
 */
void KanbanTask::setDoneVisible(const bool &visible)
{
    ui->doneButton->setVisible(visible);
}

/**
 * Hides or Shows the undone button
 * @param visible True show button | False hide button
 */
void KanbanTask::setUndoneVisible(const bool &visible)
{
    ui->undoneButton->setVisible(visible);
}

/**
 * moves the task to the right.
 */
void KanbanTask::done()
{
	store->moveTaskRight(this);
}

/**
 * Moves the task to the Left.
 */
void KanbanTask::undone()
{
	store->moveTaskLeft(this);
}

/**
 * Removes the task
 */
void KanbanTask::remove()
{
    store->removeTask(this);
	delete this;
}

/**
 * Updates properties
 */
void KanbanTask::update()
{
    rowId = store->getTaskRowId(this);
    name = store->getTaskName(this);
    position = store->getTaskPosition(this);
}
