#include <QList>

#include "kanban_window.h"
#include "ui_kanban_window.h"
#include "kanban_row.h"

/**
 * Constructor
 */
KanbanWindow::KanbanWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::KanbanWindow)
{
	ui->setupUi(this);

	newKanbanDialog = new KanbanNewKanbanDialog(this);
	newRowDialog = new KanbanNewRowDialog(this);

	store = new KanbanStore("kanban.sqlite");
	store->setup();

	setWindowTitle(store->getKanbanFirstName());
	id = store->getKanbanFirstId();

	QList<KanbanRow*> rows = store->getRows(id, this);

	for(int i = 0; i < rows.count(); i++)
	{
		ui->rowList->insertWidget(rows[i]->getPosition(), rows[i]);
		rows[i]->setLayout(ui->rowList);
	}

	for(int i = 0; i < rows.count(); i++)
	{
		rows[i]->update();
	}

	connect(ui->actionNew_Kanban, &QAction::triggered, this, &KanbanWindow::openNewKanbanDialog);
	connect(ui->actionNew_Row, &QAction::triggered, this, &KanbanWindow::openNewRowDialog);
}

/**
 * Destructor
 */
KanbanWindow::~KanbanWindow()
{
	delete ui;
}

/**
 * Opens the new knaban dialog
 */
void KanbanWindow::openNewKanbanDialog()
{
	if(newKanbanDialog->exec() == QDialog::Accepted){
		newKanban(newKanbanDialog->getName());
		newKanbanDialog->reset();
	}
}

/**
 * Opens the new row dialog.
 */
void KanbanWindow::openNewRowDialog()
{
	if(newRowDialog->exec() == QDialog::Accepted){
		newRow(newRowDialog->getName());
		newRowDialog->reset();
	}
}

/**
 * Not jet implemented
 * @param name Name of the new kanban board.
 */
void KanbanWindow::newKanban(const QString &name)
{
	qDebug() << name;
}

/**
 * Adds a new row to the current kanban board.
 * @param name name of the kanban board.
 */
void KanbanWindow::newRow(const QString &name)
{
	int size(ui->rowList->count());
	KanbanRow *newRow = store->getNewRow(this->id, size - 2, name, this);

	ui->rowList->insertWidget(size - 2, newRow);
	newRow->setLayout(ui->rowList);
}