#include <QList>
#include <QDebug>

#include "kanban_store.h"
#include "kanban_row.h"
#include "ui_kanban_row.h"
#include "kanban_window.h"
#include "kanban_task.h"


/**
 * Constructor creats the objekt sets property and hiddes some buttons if nided.
 */
KanbanRow::KanbanRow(
    const qlonglong &id,
    const qlonglong &position,
    const qlonglong &kanbanId,
    const QString &title,
    QWidget *parent) :

	QWidget(parent),
	ui(new Ui::KanbanRow)
{
	ui->setupUi(this);
    ui->groupBox->setTitle(title);

    this->id = id;
    this->position = position;
    this->kanbanId = kanbanId;
    this->name = title;


    newTaskDialog = new KanbanNewTaskDialog(this);

    if(position != 0) {
        ui->newTaskButton->setVisible(false);
    }

    if(position == 1) {
        ui->leftButton->setVisible(false);
    }

    if(title == "Open") {
        ui->deleteButton->setVisible(false);
        ui->moveWidget->setVisible(false);
    }

    if(title == "Closed") {
        ui->deleteButton->setVisible(false);
        ui->moveWidget->setVisible(false);
    }

	connect(ui->leftButton, &QPushButton::clicked, this, &KanbanRow::moveLeft);
    connect(ui->rightButton, &QPushButton::clicked, this, &KanbanRow::moveRight);
    connect(ui->newTaskButton, &QPushButton::clicked, this, &KanbanRow::openNewTaskDialog);
    connect(ui->deleteButton, &QPushButton::clicked, this, &KanbanRow::remove);
}

/**
 * Destructor.
 */
KanbanRow::~KanbanRow()
{
	delete ui;
}

/**
 * Sets the store of a row.
 * @param store Store to set.
 */
void KanbanRow::setStore(KanbanStore *store)
{
    this->store = store;

    QList<KanbanTask*> tasks = store->getTasks(id, parentWidget());

    for(int i = 0; i < tasks.count(); i++)
    {
        ui->taskList->insertWidget(0, tasks[i]);
        tasks[i]->setLayout(ui->taskList);
    }

    for(int i = 0; i < tasks.count(); i++)
    {
        tasks[i]->update();

        if(position == 0) {
            tasks[i]->setDoneVisible(true);
            tasks[i]->setUndoneVisible(false);
        } else {
            tasks[i]->setDoneVisible(true);
            tasks[i]->setUndoneVisible(true);
        }


        if(name == "Closed") {
            tasks[i]->setDoneVisible(false);
            tasks[i]->setUndoneVisible(true);
        }
    }
}

/**
 * Returns the store of a row.
 * @return Store of the row.
 */
KanbanStore* KanbanRow::getStore()
{
    return store;
}

/**
 * Returns the ID of the row.
 * @return ID of the row.
 */
qlonglong KanbanRow::getId()
{
    return id;
}

/**
 * Set the position of the row.
 * @param position Position of the row.
 */
void KanbanRow::setPosition(const qlonglong &position)
{
    this->position = position;
}

/**
 * Returns the position of the row.
 * @return Position of the row.
 */
qlonglong KanbanRow::getPosition()
{
    return position;
}

/**
 * Sets the kanban board ID in which the row is.
 * @param kanbanId kanban ID.
 */
void KanbanRow::setKanbanId(const qlonglong &kanbanId)
{
    this->kanbanId = kanbanId;
}

/**
 * Returns the kanban board id.
 * @return kanban_id
 */
qlonglong KanbanRow::getKanbanId()
{
    return kanbanId;
}

/**
 * Sets the name of a row.
 * @param name Name of the row.
 */
void KanbanRow::setName(const QString &name)
{
    this->name = name;
}

/**
 * Returns the Name of the row.
 * @return Name of the row.
 */
QString KanbanRow::getName()
{
    return name;
}

/**
 * Sets the title of the row.
 * @param title Title of the row.
 */
void KanbanRow::setTitle(const QString &title)
{
    ui->groupBox->setTitle(title);
    setName(title);
}

/**
 * Returns the title of the row.
 * @return Title of the row.
 */
QString KanbanRow::getTitle() const
{
    return ui->groupBox->title();
}

/**
 * Sets the QHBox from the kanban board to move rows in it.
 * @param layout QHBox of the kanban board window.
 */
void KanbanRow::setLayout(QBoxLayout *layout)
{
    list = layout;
}

/**
 * Adds a task to the row.
 * @param task Task to add.
 */
void KanbanRow::addTask(KanbanTask *task)
{
    ui->taskList->insertWidget(0, task);

    if(position == 0) {
        task->setDoneVisible(true);
        task->setUndoneVisible(false);
    } else {
        task->setDoneVisible(true);
        task->setUndoneVisible(true);
    }


    if(name == "Closed") {
        task->setDoneVisible(false);
        task->setUndoneVisible(true);
    }
}

/**
 * Get index of the row as in the kanban board window.
 * @return gets index in the kanban board window.
 */
int KanbanRow::getIndex()
{
    return list->indexOf(this);
}

/**
 * Sets new position in the kanban bord window.
 * @param index New index.
 */
void KanbanRow::setIndex(const int &index)
{
    list->removeWidget(this);
    list->insertWidget(index, this);
    setPosition(index);
}

/**
 * Updates the property form 
 */
void KanbanRow::update()
{
    position = store->getRowPosition(this);
    kanbanId = store->getRowKanbanId(this);
    name = store->getRowName(this);

    setIndex(position);

    if(position == 1) {
        ui->leftButton->setVisible(false);
    } else {
        ui->leftButton->setVisible(true);
    }

    if(list->count()-3 == position) {
        ui->rightButton->setVisible(false);
    } else {
        ui->rightButton->setVisible(true);
    }
}

/**
 * Move the row Left
 */
void KanbanRow::moveLeft()
{
    if(position > 1 && list->count()-2 > position ){
        store->moveRowLeft(this);
    }
}

/**
 * Move the row Right
 */
void KanbanRow::moveRight()
{
    if(list->count()-3 > position && 0 < position){
        store->moveRowRight(this);
    }
}

/**
 * Opens the new Task Dialog
 */
void KanbanRow::openNewTaskDialog()
{
    if(newTaskDialog->exec() == QDialog::Accepted){
        newTask(newTaskDialog->getName());
        newTaskDialog->reset();
    }
}

/**
 * Creates a new task
 * @param name Name of the task
 */
void KanbanRow::newTask(const QString &name){
    KanbanTask *newTask = store->getNewTask(id, name, parentWidget());

    ui->taskList->insertWidget(0, newTask);
    newTask->setLayout(ui->taskList);
}

/**
 * Removes the Row.
 */
void KanbanRow::remove()
{
    store->removeRow(this);
    delete this;
}