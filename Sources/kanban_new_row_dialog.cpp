#include "kanban_new_row_dialog.h"
#include "ui_kanban_new_row_dialog.h"

/**
 * Consturctor
 */
KanbanNewRowDialog::KanbanNewRowDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::KanbanNewRowDialog)
{
	ui->setupUi(this);
}

/**
 * Destructor
 */
KanbanNewRowDialog::~KanbanNewRowDialog()
{
	delete ui;
}

/**
 * Get the name of the new row.
 * @return Name of new row.
 */
QString KanbanNewRowDialog::getName()
{
	return ui->name->text();
}

/**
 * Restet the dialog.
 */
void KanbanNewRowDialog::reset()
{
	ui->name->clear();
}