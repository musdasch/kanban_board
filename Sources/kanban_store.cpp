#include <QWidget>
#include <QSqlQuery>
#include <QDebug>

#include "kanban_store.h"

/**
 * Constructor of the KanbanStroe setsup Database conection.
 */
KanbanStore::KanbanStore(QString name)
{
	db = QSqlDatabase::addDatabase("QSQLITE");
	db.setDatabaseName(name);
}

/**
 * Destructur sould Destroy the conection, but it dosend.
 */
KanbanStore::~KanbanStore()
{
	db.close();
	db.removeDatabase(db.connectionName());
}

/**
 * KanbanStore::setup Sets up the SQLite file with a the basic tabels kanban, row and task.
 * If kanban is empty it inserts Open and Close Rows.
 */
void KanbanStore::setup()
{
	if(db.open())
	{
		QSqlQuery query;
		
		query.prepare(
			"CREATE TABLE IF NOT EXISTS kanban ("
				"id INTEGER PRIMARY KEY,"
				"name TEXT NOT NULL"
			");"
		);
		query.exec();

		query.prepare(
			"CREATE TABLE IF NOT EXISTS row ("
				"id INTEGER PRIMARY KEY,"
				"position INTEGER NOT NULL,"
				"name TEXT NOT NULL,"
				"kanban_id INTEGER NOT NULL,"
				"FOREIGN KEY(kanban_id) REFERENCES kanban(id)"
			");"
		);
		query.exec();

		query.prepare(
			"CREATE TABLE IF NOT EXISTS task ("
				"id INTEGER PRIMARY KEY,"
				"name TEXT NOT NULL,"
				"row_id INTEGER NOT NULL,"
				"FOREIGN KEY(row_id) REFERENCES row(id)"
			");"
		);
		query.exec();

		query.prepare("SELECT count(*) from kanban;");
		if (query.exec() && query.next())
		{
			qlonglong count(query.value(0).toLongLong());
			if(count < 1)
			{
				query.prepare(
					"INSERT INTO kanban ("
						"name"
					") VALUES ("
						"\"Demo Kanban\""
					");"
						
				);
				query.exec();

				query.prepare(
					"INSERT INTO row ("
						"name,"
						"position,"
						"kanban_id"
					") VALUES ("
						"\"Open\","
						"0,"
						":kanban_id"
					");"
						
				);
				query.bindValue(":kanban_id", 1);
				query.exec();

				query.prepare(
					"INSERT INTO row ("
						"name,"
						"position,"
						"kanban_id"
					") VALUES ("
						"\"Closed\","
						"1,"
						":kanban_id"
					");"
						
				);
				query.bindValue(":kanban_id", 1);
				query.exec();
			}
		}
	}
}

/**
 * KanbanStore::getKanbanFirstId returns the first kanban id.
 * @return id of the first Kanban board.
 */
qlonglong KanbanStore::getKanbanFirstId()
{
	qlonglong id(0);
	QSqlQuery query("SELECT id FROM kanban ORDER BY id ASC LIMIT 1;");

	if(query.first())
	{
		id = query.value(0).toLongLong();
	}

	return id;
}

/**
 * Gets you access to the first kanban board name. to use if the program has freshly started
 * @return name of the first kanban board in the database.
 */
QString KanbanStore::getKanbanFirstName()
{
	QString name("");
	QSqlQuery query("SELECT name FROM kanban ORDER BY id ASC LIMIT 1;");

	if(query.first())
	{
		name = query.value(0).toString();
	}

	return name;
}

/**
 * Get the position of row as in the database. Used to update the property.
 * @param  row row to read the position in the database
 * @return     position of an row as in the databases
 */
qlonglong KanbanStore::getRowPosition(KanbanRow *row)
{
	qlonglong id = row->getId();
	return getProp(id, "position", "row").toLongLong();
}

/**
 * Returns the kanban board id as in the database. Used to update the property of a row.
 * @param  row row to read the kanban_id in the database
 * @return     kanban_id
 */
qlonglong KanbanStore::getRowKanbanId(KanbanRow *row)
{
	qlonglong id = row->getId();
	return getProp(id, "kanban_id", "row").toLongLong();
}

/**
 * Returns the name of a row as in the database. Used to update the property of a row.
 * @param  row row to read the name in the database
 * @return     name of the row
 */
QString KanbanStore::getRowName(KanbanRow *row)
{
	qlonglong id = row->getId();
	return getProp(id, "name", "row").toString();
}

/**
 * [KanbanStore::getNewRow description]
 * @param  kanban_id id of the kanban board to add the new row.
 * @param  position  position to add it at.
 * @param  name      name of the new row.
 * @param  parent    parend widget
 * @return           new KanbanRow
 */
KanbanRow* KanbanStore::getNewRow(
	const qlonglong &kanban_id,
	const qlonglong &position,
	const QString &name,
	QWidget *parent)
{

	QSqlQuery query;

	query.prepare("UPDATE row SET position=position+1 WHERE position>=:position");
	query.bindValue(":position", position);
	query.exec();

	query.prepare("INSERT INTO row (position, name, kanban_id) VALUES (:position, :name, :kanban_id)");
	query.bindValue(":position", position);
	query.bindValue(":name", name);
	query.bindValue(":kanban_id", kanban_id);
	query.exec();

	KanbanRow *newRow = new KanbanRow(
		query.lastInsertId().toLongLong(),
		position,
		kanban_id,
		name, parent);
	newRow->setStore(this);

	rows.append(newRow);

	return newRow;
}

/**
 * Gets all rows of a kanban board.
 */
QList<KanbanRow*> KanbanStore::getRows(const qlonglong &kanban_id, QWidget *parent)
{

	QSqlQuery query;

	query.prepare("SELECT id, position, name FROM row WHERE kanban_id=:id ORDER BY position ASC;");
	query.bindValue(":id", kanban_id);
	query.exec();

	while (query.next()) {
		qlonglong id = query.value(0).toLongLong();
		qlonglong position = query.value(1).toLongLong();
		QString name = query.value(2).toString();

		KanbanRow *newRow = new KanbanRow(id, position, kanban_id, name, parent);
		newRow->setStore(this);
		rows.append(newRow);
	}

	return rows;
}

/**
 * move a row to the left in the kanban board.
 * @param row row to move.
 */
void KanbanStore::moveRowLeft(KanbanRow *row)
{
	qlonglong id = row->getId();
	qlonglong position = row->getPosition();

	QSqlQuery query;

	query.prepare("UPDATE row SET position=position+1 WHERE position=:position-1");
	query.bindValue(":position", position);
	query.exec();

	query.prepare("UPDATE row SET position=position-1 WHERE id=:id");
	query.bindValue(":id", id);
	query.exec();

	updateRows();
}

/**
 * Move a row to the right.
 * @param row Row to move
 */
void KanbanStore::moveRowRight(KanbanRow *row)
{
	qlonglong id = row->getId();
	qlonglong position = row->getPosition();

	QSqlQuery query;

	query.prepare("UPDATE row SET position=position-1 WHERE position=:position+1");
	query.bindValue(":position", position);
	query.exec();

	query.prepare("UPDATE row SET position=position+1 WHERE id=:id");
	query.bindValue(":id", id);
	query.exec();

	updateRows();
}

/**
 * Remove a Row from the kanban board.
 * @param row Row to remove.
 */
void KanbanStore::removeRow(KanbanRow *row)
{
	qlonglong id = row->getId();
	qlonglong position = row->getPosition();

	QSqlQuery query;

	query.prepare("UPDATE row SET position=position-1 WHERE position>=:position");
	query.bindValue(":position", position);
	query.exec();

	query.prepare("DELETE FROM row WHERE id=:id");
	query.bindValue(":id", id);
	query.exec();

	rows.removeAt(rows.indexOf(row));

	updateRows();
}

/**
 * Get id of a task as in the databases. Used to update property.
 * @param  task Task of which to get the id from
 * @return      id as in the database.
 */
qlonglong KanbanStore::getTaskRowId(KanbanTask *task)
{
	qlonglong id = task->getId();
	return getProp(id, "row_id", "task").toLongLong();
}

/**
 * Get the position of a row in which the task is.
 * @param  task Task to get the position from.
 * @return      position of row in which the task is.
 */
qlonglong KanbanStore::getTaskPosition(KanbanTask *task)
{
	qlonglong id(task->getId());
	qlonglong position(0);

	QSqlQuery query;

	query.prepare(
		"SELECT row.position"
		" FROM task"
		" INNER JOIN row ON task.row_id=row.id WHERE task.id=:id"
	);
	query.bindValue(":id", id);
	query.exec();

	if(query.first()) {
		position = query.value(0).toLongLong();
	}

	return position;
}


/**
 * Get the name of the task as in the database. Used to update property.
 * @param  task Tast form which to get the name in the database.
 * @return      Name of task.
 */
QString KanbanStore::getTaskName(KanbanTask *task)
{
	qlonglong id = task->getId();
	return getProp(id, "name", "task").toString();
}


/**
 * Creat a new Task in the database and a Object.
 * @param  rowId  ID of the row.
 * @param  name   Name of the task.
 * @param  parent parent widget
 * @return        New task.
 */
KanbanTask* KanbanStore::getNewTask(const qlonglong &rowId, const QString &name, QWidget *parent )
{
	QSqlQuery query;

	query.prepare("INSERT INTO task (name, row_id) VALUES (:name, :row_id)");
	query.bindValue(":name", name);
	query.bindValue(":row_id", rowId);
	query.exec();

	KanbanTask *newTask = new KanbanTask(
		query.lastInsertId().toLongLong(),
		rowId,
		name,
		parent);
	newTask->setStore(this);

	tasks.append(newTask);

	return newTask;
}

/**
 * Returns all Tasks of a row.
 */
QList<KanbanTask*> KanbanStore::getTasks(const qlonglong &rowId, QWidget *parent)
{
	QSqlQuery query;

	QList<KanbanTask*> outTasks;

	query.prepare("SELECT id, name FROM task WHERE row_id=:id ORDER BY id ASC;");
	query.bindValue(":id", rowId);
	query.exec();

	while (query.next()) {
		qlonglong id = query.value(0).toLongLong();
		QString name = query.value(1).toString();

		KanbanTask *newTask = new KanbanTask(id, rowId, name, parent);
		newTask->setStore(this);
		tasks.append(newTask);
		outTasks.append(newTask);
	}

	return outTasks;
}

/**
 * Moves a task to the left.
 * @param task Task to move.
 */
void KanbanStore::moveTaskLeft(KanbanTask *task)
{
	qlonglong id(task->getId());
	qlonglong rowId(task->getRowId());
	qlonglong position(task->getPosition());

	QSqlQuery query;

	query.prepare("SELECT id FROM row WHERE position=:position-1 LIMIT 1");
	query.bindValue(":position", position);
	query.exec();

	if(query.first()) {
		rowId = query.value(0).toLongLong();
	}

	query.prepare("UPDATE task SET row_id=:rowId WHERE id=:id");
	query.bindValue(":rowId", rowId);
	query.bindValue(":id", id);
	query.exec();


	for(int i = 0; i < rows.count(); i++)
	{
		if(rows[i]->getId() == rowId){
			rows[i]->addTask(task);
			break;
		}
	}

	task->update();
}

/**
 * Moves a task to the right.
 * @param task Task to move.
 */
void KanbanStore::moveTaskRight(KanbanTask *task)
{
	qlonglong id(task->getId());
	qlonglong rowId(task->getRowId());
	qlonglong position(task->getPosition());

	QSqlQuery query;

	query.prepare("SELECT id FROM row WHERE position=:position+1 LIMIT 1");
	query.bindValue(":position", position);
	query.exec();

	if(query.first()) {
		rowId = query.value(0).toLongLong();
	}

	query.prepare("UPDATE task SET row_id=:rowId WHERE id=:id");
	query.bindValue(":rowId", rowId);
	query.bindValue(":id", id);
	query.exec();


	for(int i = 0; i < rows.count(); i++)
	{
		if(rows[i]->getId() == rowId){
			rows[i]->addTask(task);
			break;
		}
	}

	task->update();
}

/**
 * Removes a task form the database.
 * @param task Task to remove.
 */
void KanbanStore::removeTask(KanbanTask *task)
{
	qlonglong id = task->getId();
	QSqlQuery query;

	query.prepare("DELETE FROM task WHERE id=:id");
	query.bindValue(":id", id);
	query.exec();

	tasks.removeAt(tasks.indexOf(task));
}

/**
 * Returns property from the database by id.
 * @param  id    ID of the property.
 * @param  table Table of the property.
 * @param  prop  name of the property.
 * @return       Property as variant.
 */
QVariant KanbanStore::getProp(
	const qlonglong &id,
	const QString &prop,
	const QString &table)
{
	QSqlQuery query;
	QVariant out;

	query.prepare("SELECT " + prop + " FROM " + table + " WHERE id=:id");
	query.bindValue(":id", id);
	query.exec();

	if(query.first()){
		out = query.value(0);
	}

	return out;
}

/**
 * Updats all rows.
 */
void KanbanStore::updateRows()
{
	for(int i = 0; i < rows.count(); i++)
	{
		rows[i]->update();
	}
}

/**
 * Updates all tasks
 */
void KanbanStore::updateTasks()
{
	for(int i = 0; i < tasks.count(); i++)
	{
		tasks[i]->update();
	}
}