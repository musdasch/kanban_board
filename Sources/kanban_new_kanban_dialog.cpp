#include "kanban_new_kanban_dialog.h"
#include "ui_kanban_new_kanban_dialog.h"

/**
 * Consturctor
 */
KanbanNewKanbanDialog::KanbanNewKanbanDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::KanbanNewKanbanDialog)
{
	ui->setupUi(this);
}

/**
 * Desturktor.
 */
KanbanNewKanbanDialog::~KanbanNewKanbanDialog()
{
	delete ui;
}

/**
 * Returns name as entert in the fild name.
 * @return name of kanban board.
 */
QString KanbanNewKanbanDialog::getName()
{
	return ui->name->text();
}

/**
 * Resets dialog.
 */
void KanbanNewKanbanDialog::reset()
{
	ui->name->clear();
}