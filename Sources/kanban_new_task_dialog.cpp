#include "kanban_new_task_dialog.h"
#include "ui_kanban_new_task_dialog.h"

/**
 * Constructor
 */
KanbanNewTaskDialog::KanbanNewTaskDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::KanbanNewTaskDialog)
{
	ui->setupUi(this);
}

/**
 * Destructor
 */
KanbanNewTaskDialog::~KanbanNewTaskDialog()
{
	delete ui;
}

/**
 * Returns name of new task.
 * @return Name of new task.
 */
QString KanbanNewTaskDialog::getName()
{
	return ui->name->text();
}

/**
 * Resets the dialog
 */
void KanbanNewTaskDialog::reset()
{
	ui->name->clear();
}