#include <QtTest>
#include "test_suite.h"

/**
 * Starts the test
 * @param  argc Some arguments
 * @param  argv Some arguments
 * @return      Error code
 */
int main(int argc, char** argv)
{
	QApplication app(argc, argv);

	TestSuite testsuite;

	return QTest::qExec(&testsuite, argc, argv);
}