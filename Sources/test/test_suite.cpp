#include <QList>
#include <QDebug>

#include "test_suite.h"

/**
 * Consturctor
 */
TestSuite::TestSuite() : QObject(nullptr) {}

/**
 * Before all tests
 */
void TestSuite::initTestCase()
{
}

/**
 * After all tests
 */
void TestSuite::cleanupTestCase()
{
}


/**
 * Before test new store in memory.
 */
void TestSuite::init()
{
	store_ = new KanbanStore(":memory:");
}

/**
 * Clean up store
 */
void TestSuite::cleanup()
{
	delete store_;
	store_ = 0;
}

/**
 * Test the setup of the database.
 */
void TestSuite::testStoreSetup()
{
	store_->setup();

	QCOMPARE(store_->getKanbanFirstName(), QString("Demo Kanban"));
	QCOMPARE(store_->getKanbanFirstId(), 1);
}

/**
 * Test to get rows of a kanban board.
 */
void TestSuite::testStoreGetRows()
{
	store_->setup();

	QList<KanbanRow*> rows = store_->getRows(1, nullptr);

	QCOMPARE(rows.count(), 2);


	QCOMPARE(rows[0]->getId(), 1);
	QCOMPARE(rows[0]->getPosition(), 0);
	QCOMPARE(rows[0]->getKanbanId(), 1);
	QCOMPARE(rows[0]->getName(), "Open");

	QCOMPARE(rows[1]->getId(), 2);
	QCOMPARE(rows[1]->getPosition(), 1);
	QCOMPARE(rows[1]->getKanbanId(), 1);
	QCOMPARE(rows[1]->getName(), "Closed");

	delete rows[0];
	delete rows[1];
}

/**
 * Tests if new Row can be created with store.
 */
void TestSuite::testStoreGetNewRow()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 1, "Test", nullptr);

	QCOMPARE(row->getId(), 3);
	QCOMPARE(row->getPosition(), 1);
	QCOMPARE(row->getKanbanId(), 1);
	QCOMPARE(row->getName(), "Test");

	delete row;
}

/**
 * Tests if postition of Row is equal to database.
 */
void TestSuite::testStoreGetRowPosition()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);

	QCOMPARE(store_->getRowPosition(row), 2);

	delete row;
}

/**
 * Tests if kanban ID is equal to database.
 */
void TestSuite::testStoreGetRowKanbanId()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);

	QCOMPARE(store_->getRowKanbanId(row), 1);

	delete row;
}

/**
 * Tests if row name is equal to database.
 */
void TestSuite::testStoreGetRowName()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);

	QCOMPARE(store_->getRowName(row), "Test");

	delete row;
}

/**
 * [TestSuite::testStoreGetNewTask description]
 */
void TestSuite::testStoreGetNewTask()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "Test Task", nullptr);

	QCOMPARE(task->getId(), 1);
	QCOMPARE(task->getRowId(), 1);
	QCOMPARE(task->getName(), "Test Task");

	delete task;
}

/**
 * [TestSuite::testStoreGetTasks description]
 */
void TestSuite::testStoreGetTasks()
{
	store_->setup();

	KanbanTask* task1 = store_->getNewTask(1, "1# Test Task", nullptr);
	KanbanTask* task2 = store_->getNewTask(1, "2# Test Task", nullptr);

	QList<KanbanTask*> tasks = store_->getTasks(1, nullptr);

	QCOMPARE(tasks.count(), 2);

	QCOMPARE(tasks[0]->getId(), task1->getId());
	QCOMPARE(tasks[0]->getRowId(), task1->getRowId());
	QCOMPARE(tasks[0]->getName(), task1->getName());

	QCOMPARE(tasks[1]->getId(), task2->getId());
	QCOMPARE(tasks[1]->getRowId(), task2->getRowId());
	QCOMPARE(tasks[1]->getName(), task2->getName());


	delete task1;
	delete task2;
	delete tasks[0];
	delete tasks[1];
}

/**
 * [TestSuite::testStoreGetTaskRowId description]
 */
void TestSuite::testStoreGetTaskRowId()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);

	QCOMPARE(store_->getTaskRowId(task), 1);

	delete task;
}

/**
 * [TestSuite::testStoreGetTaskPosition description]
 */
void TestSuite::testStoreGetTaskPosition()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);

	QCOMPARE(store_->getTaskPosition(task), 0);

	delete task;
}

/**
 * [TestSuite::testStoreGetTaskName description]
 */
void TestSuite::testStoreGetTaskName()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);

	QCOMPARE(store_->getTaskName(task), "1# Test Task");

	delete task;
}

/**
 * [TestSuite::testStoreRemoveTask description]
 */
void TestSuite::testStoreRemoveTask()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);

	QCOMPARE(store_->getTaskName(task), "1# Test Task");

	store_->removeTask(task);

	QCOMPARE(store_->getTaskName(task), "");

	delete task;
}

/**
 * [TestSuite::testRowGetId description]
 */
void TestSuite::testRowGetId()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);

	QCOMPARE(row->getId(), 3);

	delete row;
}


/**
 * [TestSuite::testStoreGetRowPosition description]
 */
void TestSuite::testRowGetPosition()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);

	QCOMPARE(row->getPosition(), 2);

	delete row;
}

/**
 * [TestSuite::testStoreGetRowKanbanId description]
 */
void TestSuite::testRowGetKanbanId()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);

	QCOMPARE(row->getKanbanId(), 1);

	delete row;
}

/**
 * [TestSuite::testStoreGetRowKanbanId description]
 */
void TestSuite::testRowGetName()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);

	QCOMPARE(row->getName(), "Test");

	delete row;
}

/**
 * [TestSuite::testStoreGetRowKanbanId description]
 */
void TestSuite::testRowGetTitle()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);

	QCOMPARE(row->getTitle(), "Test");

	delete row;
}

/**
 * [TestSuite::testStoreGetRowPosition description]
 */
void TestSuite::testRowSetPosition()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);
	row->setPosition(100);

	QCOMPARE(row->getPosition(), 100);

	delete row;
}

/**
 * [TestSuite::testStoreGetRowKanbanId description]
 */
void TestSuite::testRowSetKanbanId()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);
	row->setKanbanId(100);

	QCOMPARE(row->getKanbanId(), 100);

	delete row;
}

/**
 * [TestSuite::testStoreGetRowKanbanId description]
 */
void TestSuite::testRowSetName()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);
	row->setName("newTest");

	QCOMPARE(row->getName(), "newTest");

	delete row;
}

/**
 * [TestSuite::testStoreGetRowKanbanId description]
 */
void TestSuite::testRowSetTitle()
{
	store_->setup();

	KanbanRow* row = store_->getNewRow(1, 2, "Test", nullptr);
	row->setTitle("newTest");

	QCOMPARE(row->getTitle(), "newTest");

	delete row;
}

/**
 * [TestSuite::testTaskGetId description]
 */
void TestSuite::testTaskGetId()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);

	QCOMPARE(task->getId(), 1);

	delete task;
}

/**
 * [TestSuite::testTaskGetRowId description]
 */
void TestSuite::testTaskGetRowId()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);

	QCOMPARE(task->getRowId(), 1);

	delete task;
}

/**
 * [TestSuite::testStoreGetTaskPosition description]
 */
void TestSuite::testTaskGetPosition()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);

	QCOMPARE(task->getPosition(), 0);

	delete task;
}

/**
 * [TestSuite::testStoreGetTaskName description]
 */
void TestSuite::testTaskGetName()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);

	QCOMPARE(task->getName(), "1# Test Task");

	delete task;
}

/**
 * [TestSuite::testTaskGetRowId description]
 */
void TestSuite::testTaskSetRowId()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);
	task->setRowId(100);

	QCOMPARE(task->getRowId(), 100);

	delete task;
}

/**
 * [TestSuite::testStoreGetTaskPosition description]
 */
void TestSuite::testTaskSetPosition()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);
	task->setPosition(100);

	QCOMPARE(task->getPosition(), 100);

	delete task;
}

/**
 * [TestSuite::testStoreGetTaskName description]
 */
void TestSuite::testTaskSetName()
{
	store_->setup();

	KanbanTask* task = store_->getNewTask(1, "1# Test Task", nullptr);
	task->setName("newTest");

	QCOMPARE(task->getName(), "newTest");

	delete task;
}
