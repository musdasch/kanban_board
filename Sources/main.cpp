// main.cpp
#include <QApplication>
#include "kanban_window.h"

/**
 * Stats the program.
 * @param  argc Some arguments to start
 * @param  argv Some arguments to start
 * @return      Error code
 */
int main(int argc, char** argv)
{
	QApplication app(argc, argv);
	KanbanWindow kanbanWindow;
	kanbanWindow.show();

	return app.exec();
}