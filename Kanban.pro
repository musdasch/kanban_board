#--------------------------------------------------------------
#
# Kanban Board Project for HF-ICT
#
#--------------------------------------------------------------

QT += core gui widgets sql

CONFIG += sql

#greaterThan(QT_MAYJOR_VERSION, 4): QT += widgets

TARGET = kanban
TEMPLATE = app

VERSION = 0.1

DEFINES += QT_DEPRECATED_WARNINGS \
    APP_VERSION=\\\"$$VERSION\\\"

INCLUDEPATH += $$PWD/Headers

test {
    message(Test build)

    QT += testlib
    TARGET = kanban-test
    INCLUDEPATH += $$PWD/Headers/test

    SOURCES += \
        Sources/test/main.cpp \
        Sources/test/test_suite.cpp \
        Sources/kanban_store.cpp \
        Sources/kanban_window.cpp \
        Sources/kanban_row.cpp \
        Sources/kanban_task.cpp \
        Sources/kanban_new_kanban_dialog.cpp \
        Sources/kanban_new_row_dialog.cpp \
        Sources/kanban_new_task_dialog.cpp \

    HEADERS += \
        Headers/test/test_suite.h \
        Headers/kanban_window.h \
        Headers/kanban_store.h \
        Headers/kanban_row.h \
        Headers/kanban_task.h \
        Headers/kanban_new_kanban_dialog.h \
        Headers/kanban_new_row_dialog.h \
        Headers/kanban_new_task_dialog.h \

    FORMS +=  \
        UI/kanban_window.ui \
        UI/kanban_row.ui \    
        UI/kanban_task.ui \    
        UI/kanban_new_kanban_dialog.ui \    
        UI/kanban_new_row_dialog.ui \    
        UI/kanban_new_task_dialog.ui \  

} else {
    message(Normal build)
    
    SOURCES += \
        Sources/main.cpp \
        Sources/kanban_store.cpp \
        Sources/kanban_window.cpp \
        Sources/kanban_row.cpp \
        Sources/kanban_task.cpp \
        Sources/kanban_new_kanban_dialog.cpp \
        Sources/kanban_new_row_dialog.cpp \
        Sources/kanban_new_task_dialog.cpp \

    HEADERS += \
        Headers/kanban_window.h \
        Headers/kanban_store.h \
        Headers/kanban_row.h \
        Headers/kanban_task.h \
        Headers/kanban_new_kanban_dialog.h \
        Headers/kanban_new_row_dialog.h \
        Headers/kanban_new_task_dialog.h \

    FORMS +=  \
        UI/kanban_window.ui \
        UI/kanban_row.ui \    
        UI/kanban_task.ui \    
        UI/kanban_new_kanban_dialog.ui \    
        UI/kanban_new_row_dialog.ui \    
        UI/kanban_new_task_dialog.ui \    
}
