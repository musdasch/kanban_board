# Offline Kanban Board
![alt text](program.png "Kanban Board")

The Kanban Board is a simple program with a swim line like structure along the lines of Trello or the GitLab Issue board. It's intended to use in an offline environment and stores it's data in a SQLite3 database file.

The create new kanban function is not implemented yet, so you can not create new boards, switch between them or remove them.

However, you can create/remove new row and task and move the tasks between the rows with the buttons done and undone.

## Build and Start Instructions

### Build Requirements
Before you can build anything you have to install the the basic build tools.

On Debian:
```
#> sudo apt install build-essential qt5-default
```

### Build Kanban Board
In order to build the program you have to download the source from the git repository. If you already have build it pleas make sure you run `make clean` before you run make again.

After you have downloaded the sources you have to create you make file with `qmake`.

Then you can use make to build the program and start it afterwords.

On Debian:
```
#> git clone https://gitlab.com/musdasch/kanban_board.git
#> cd kanban_board
#> qmake
#> make
#> ./kanban
```



### Build Kanban Board Tests
In order to build the tests of the program you have to download the source from the git repository. If you already have build it pleas make sure you run `make clean` before you run make again.

After you have downloaded the sources you have to create you make file with `qmake`.

Then you can use make to build the program and start it afterwords.

On Debian:
```
#> git clone https://gitlab.com/musdasch/kanban_board.git
#> cd kanban_board
#> qmake "CONFIG += test"
#> make
#> ./kanban-test
```

## Thoughts about the project
My plan was it to build a simple kanban board software for offline use with Qt and QxOrm as an ORM, but after two days of trying to get QxOrm, ODB, SQLite ORM or some of the dozens ORMs on GitHub to get working I scraped the ORM part and tried to get it done with the SQL Driver from Qt.

That did kill some of my structure I had planned, but that's how life goes.

## Things I would do if I had more time
- Implement the function to add, remove and switch between boards
- Use a Object Relastion
- Write mocs for testing
- Document the code
