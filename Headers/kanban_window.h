#ifndef KANBAN_WINDOW_H
#define KANBAN_WINDOW_H

#include <QMainWindow>
#include <QDebug>

#include "kanban_store.h"
#include "kanban_new_kanban_dialog.h"
#include "kanban_new_row_dialog.h"

namespace Ui {
	class KanbanWindow;
}

/**
 * The window and the kanban board.
 */
class KanbanWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit KanbanWindow(QWidget *parent = nullptr);
	~KanbanWindow();


private slots:
	void openNewKanbanDialog();
	void openNewRowDialog();

private:
	KanbanStore *store;
	qlonglong id;
	KanbanNewKanbanDialog *newKanbanDialog;
	KanbanNewRowDialog *newRowDialog;
	Ui::KanbanWindow *ui;

	void newKanban(const QString &);
	void newRow(const QString &);


};

#endif
