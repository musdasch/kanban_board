/**
 * KanbanStore Startet as sort of an salf made ORM, but it makes more. So do not do this.
 */
#ifndef KANBAN_STORE_H
#define KANBAN_STORE_H

#include <QSqlDatabase>
#include <QList>

#include "kanban_row.h"
#include "kanban_task.h"

class KanbanStore
{

public:
	KanbanStore(QString);
	~KanbanStore();
	void setup();
	qlonglong getKanbanFirstId();
	QString getKanbanFirstName();

	qlonglong getRowPosition(KanbanRow *);
	qlonglong getRowKanbanId(KanbanRow *);
	QString getRowName(KanbanRow *);
	
	KanbanRow* getNewRow(
		const qlonglong &,
		const qlonglong &,
		const QString &,
		QWidget *);
	QList<KanbanRow*> getRows(const qlonglong &, QWidget *);
	
	void moveRowLeft(KanbanRow *);
	void moveRowRight(KanbanRow *);
	void removeRow(KanbanRow *);

	qlonglong getTaskRowId(KanbanTask *);
	qlonglong getTaskPosition(KanbanTask *);
	QString getTaskName(KanbanTask *);

	KanbanTask* getNewTask(
		const qlonglong &,
		const QString&,
		QWidget *);
	QList<KanbanTask*> getTasks(const qlonglong &, QWidget *);

	void moveTaskLeft(KanbanTask *);
	void moveTaskRight(KanbanTask *);
	void removeTask(KanbanTask *);




private:
	QSqlDatabase db;
	QList<KanbanRow*> rows;
	QList<KanbanTask*> tasks;

	QVariant getProp(
		const qlonglong &,
		const QString &,
		const QString &);
	void updateRows();
	void updateTasks();
	
};

#endif