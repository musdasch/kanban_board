#ifndef KANBAN_TASK_H
#define KANBAN_TASK_H

#include <QWidget>
#include <QBoxLayout>

#include "kanban_new_task_dialog.h"

/**
 * Hack to get KanbanStore in to row.
 */
class KanbanStore;

namespace Ui {
    class KanbanTask;
}

/**
 * Task in a kanban Bord.
 */
class KanbanTask : public QWidget
{
    Q_OBJECT

public:
    explicit KanbanTask(const qlonglong &, const qlonglong &, const QString &, QWidget *parent = nullptr);
    ~KanbanTask();

    void setStore(KanbanStore *);
    KanbanStore* getStore();

    qlonglong getId() const;

    void setRowId(const qlonglong &);
    qlonglong getRowId() const;

    void setPosition(const qlonglong &);
    qlonglong getPosition() const;

    void setName(const QString &);
    QString getName() const;

    void setLayout(QBoxLayout *);

    void setDoneVisible(const bool &);
    void setUndoneVisible(const bool &);

    void update();

private slots:
    void done();
    void undone();
    void remove();

private:
    KanbanStore *store;
    qlonglong id;
    qlonglong rowId;
    qlonglong position;
    QString name;

    QBoxLayout *list;
    Ui::KanbanTask *ui;
};

#endif
