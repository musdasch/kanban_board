#ifndef KANBAN_NEW_KANBAN_DIALOG_H
#define KANBAN_NEW_KANBAN_DIALOG_H

#include <QtWidgets/QDialog>

namespace Ui {
    class KanbanNewKanbanDialog;
}

/**
 * Dialog to create a new kanban board.
 */
class KanbanNewKanbanDialog : public QDialog
{
    Q_OBJECT

public:
    explicit KanbanNewKanbanDialog(QWidget *parent = nullptr);
    ~KanbanNewKanbanDialog();
    QString getName();
    void reset();

private:
    Ui::KanbanNewKanbanDialog *ui;
};

#endif