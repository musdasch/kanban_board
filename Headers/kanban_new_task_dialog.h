#ifndef KANBAN_NEW_TASK_DIALOG_H
#define KANBAN_NEW_TASK_DIALOG_H

#include <QtWidgets/QDialog>

namespace Ui {
    class KanbanNewTaskDialog;
}

/**
 * Dialog to create new Task.
 */
class KanbanNewTaskDialog : public QDialog
{
    Q_OBJECT

public:
    explicit KanbanNewTaskDialog(QWidget *parent = nullptr);
    ~KanbanNewTaskDialog();
    QString getName();
    void reset();

private:
    Ui::KanbanNewTaskDialog *ui;
};

#endif