#ifndef KANBAN_NEW_ROW_DIALOG_H
#define KANBAN_NEW_ROW_DIALOG_H

#include <QtWidgets/QDialog>

namespace Ui {
    class KanbanNewRowDialog;
}

/**
 * Dialog to create a new row.
 */
class KanbanNewRowDialog : public QDialog
{
    Q_OBJECT

public:
    explicit KanbanNewRowDialog(QWidget *parent = nullptr);
    ~KanbanNewRowDialog();
    QString getName();
    void reset();

private:
    Ui::KanbanNewRowDialog *ui;
};

#endif