#include <QtTest/QtTest>

#include "kanban_store.h"
#include "kanban_window.h"
#include "kanban_row.h"
#include "kanban_task.h"

/**
 * The test suit holds all tests.
 */
class TestSuite: public QObject
{
    Q_OBJECT

    KanbanStore *store_;

public:
    TestSuite();

private slots:
    void initTestCase();
    void cleanupTestCase();

    void init();
    void cleanup();

    void testStoreSetup();
    void testStoreGetRows();
    void testStoreGetNewRow();
    void testStoreGetRowPosition();
    void testStoreGetRowKanbanId();
    void testStoreGetRowName();
    void testStoreGetNewTask();
    void testStoreGetTasks();
    void testStoreGetTaskRowId();
    void testStoreGetTaskPosition();
    void testStoreGetTaskName();
    void testStoreRemoveTask();
    void testRowGetId();
    void testRowGetPosition();
    void testRowGetKanbanId();
    void testRowGetName();
    void testRowGetTitle();
    void testRowSetPosition();
    void testRowSetKanbanId();
    void testRowSetName();
    void testRowSetTitle();
    void testTaskGetId();
    void testTaskGetRowId();
    void testTaskGetPosition();
    void testTaskGetName();
    void testTaskSetRowId();
    void testTaskSetPosition();
    void testTaskSetName(); 
};