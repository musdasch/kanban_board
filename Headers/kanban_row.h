#ifndef KANBAN_ROW_H
#define KANBAN_ROW_H

#include <QWidget>
#include <QBoxLayout>

#include "kanban_new_task_dialog.h"
#include "kanban_task.h"

/**
 * Hack to get KanbanStore in to row.
 */
class KanbanStore;

namespace Ui {
    class KanbanRow;
}

/**
 * Row in the Kanban Board.
 */
class KanbanRow : public QWidget
{
    Q_OBJECT

public:
    explicit KanbanRow(
        const qlonglong &,
        const qlonglong &,
        const qlonglong &,
        const QString &,
        QWidget *parent = nullptr);
    ~KanbanRow();

    void setStore(KanbanStore *);
    KanbanStore* getStore();

    qlonglong getId();

    void setPosition(const qlonglong &);
    qlonglong getPosition();

    void setKanbanId(const qlonglong &);
    qlonglong getKanbanId();

    void setName(const QString &);
    QString getName();
    
    void setTitle(const QString &);
    QString getTitle() const;

    void setLayout(QBoxLayout *);

    void addTask(KanbanTask *);

    int getIndex();
    void setIndex(const int &);

    void update();

private slots:
    void moveLeft();
    void moveRight();
    void openNewTaskDialog();
    void remove();

private:
    KanbanStore *store;
    qlonglong id;
    qlonglong position;
    qlonglong kanbanId;
    QString name;

    KanbanNewTaskDialog *newTaskDialog;
    QBoxLayout *list;
    Ui::KanbanRow *ui;

    void newTask(const QString &);
};

#endif
